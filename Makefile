# db and redis run
compose=docker-compose -f docker-compose.development.all.yml
# args=--service-ports
args=${args}

all:
	$(compose) build

# DB and Extra Services Related
extras:
	$(compose) up -d postgresql redis
ironman_db:
	$(compose) run --rm ironman bundle exec rake db:create
migrate_ironman_db:
	#$(compose) run --rm ironman bundle exec rake db:migrate
	$(compose) run --rm ironman bundle exec rake db:structure:load
migrate_ironman_testdb:
	$(compose) run --rm ironman bundle exec rake db:migrate RAILS_ENV=test
migrate_ops_db:
	$(compose) run --rm ops-center bundle exec rake db:migrate RAILS_ENV=development
seed:
	$(compose) run --rm ironman bundle exec rake db:seed
	$(compose) run --rm ironman bundle exec rake pp:demodata RAILS_ENV=development
	$(compose) run --rm ironman bundle exec rake pp:demodata RAILS_ENV=test
up:
	$(compose) up
down:
	$(compose) down
stop:
	$(compose) stop

# Attaching to containers for binding.pry
ops_attach:
	docker attach $(docker ps -a | grep workspace_ops-center | awk '{print $1}')
ironman_attach:
	docker attach `docker ps -a | awk '{ print $$1, $$2}' | grep workspace_ironman | awk '{ print $$1}'`

# Docker-Compose Command Toolbox
## batch app listening in port 3001
ops-center:
	$(compose) build ops-center
test_ops-center:
	$(compose) run ops-center rspec
drop_ops-center_db:
	$(compose) run --rm ops-center bundle exec rake db:drop

## ironman app listening in port 3000
ironman:
	$(compose) build ironman
test_ironman:
	$(compose) run --rm ironman rspec
drop_ironman_db:
	$(compose) run --rm ironman bundle exec rake db:drop

## management front-end app listening in port XXXX
dashboard:
	$(compose) build dashboard

## console
ironman_console:
	$(compose) run --rm ironman bundle exec rails console
ops-center_console:
	$(compose) run --rm ops-center bundle exec rails console

## bash
ironman_bash:
	$(compose) run --rm ironman bash
ops-center_bash:
	$(compose) run --rm ops-center bash

## postgresql psql
psql_bash:
	$(compose) run --rm postgresql bash
ps:
	$(compose) ps
prune:
	docker system prune -af
