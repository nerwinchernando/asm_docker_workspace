FROM ruby:2.3.8-alpine3.7

ARG VPC_PROXY \
    TEST_ENV_NUMBER
ENV http_proxy ${VPC_PROXY} \
    TEST_ENV_NUMBER ${TEST_ENV_NUMBER}

WORKDIR /app
COPY Gemfile Gemfile.lock /app/

RUN sed -i 's/http\:\/\/dl-cdn.alpinelinux.org/https\:\/\/alpine.global.ssl.fastly.net/g' /etc/apk/repositories
RUN apk --update --no-cache add \
    build-base \
    postgresql \
    postgresql-dev \
    postgresql-client \
    jq \
    bash \
    gnupg \
    && adduser -S -D -h /home/ironman -s /bin/ash ironman \
    && gem install bundler --pre \
    && bundle install \
    && bundle binstubs rake --force

COPY . .

# Important otherwise puma silently exits
RUN mkdir -p /app/tmp \
  && chown -R ironman /app

USER ironman

EXPOSE 3000

# By default, run ironman in puma, but can override, e.g.
# $ docker run ironman bundle exec rails c'

CMD [ "bundle", "exec", "puma", "-C", "config/puma.rb", "-b", "tcp://0.0.0.0:3000" ]
